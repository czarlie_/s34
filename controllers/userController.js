// User model
const { model } = require('mongoose')
const auth = require('./../auth')
const User = require('../models/User')
const bcrypt = require('bcrypt')
const { JsonWebTokenError } = require('jsonwebtoken')
const { createAccessToken } = require('../auth')

module.exports.getAllUsers = () => {
  // difference between findOne() & find()
  // findOne() returns one document
  // find(query, fieldprojection) return an array of documents []
  return User.find().then((result, error) => {
    return result !== null ? result : error
  })
}

module.exports.getUserProfile = (reqBody) => {
  return User.findById({ _id: reqBody.id }).then((result) => {
    return result
  })
}

module.exports.checkEmail = async (email) => {
  return User.findOne({ email: email }).then((result, error) => {
    console.log(result) //null

    if (result !== null) {
      return false
    } else {
      if (result === null) {
        //send back the response to the client
        return true
      } else {
        return error
      }
    }
  })
}

module.exports.registerUser = (reqBody) => {
  // save/create a new user document
  // using .save() method to save document to the database
  // console.log(reqBody)

  // how to use object destructuring
  // why? to make distinct variables for each property w/o using dot notation
  // const {properties} = < object reference >
  const { firstname, lastname, email, password, mobileNo, age } = reqBody
  // console.log(firstName)

  const newUser = new User({
    firstname: firstname,
    lastname: lastname,
    email: email,
    password: bcrypt.hashSync(password, 10),
    mobileNo: mobileNo,
    age: age
  })

  // save the newUser object to the database
  return newUser.save().then((result, error) => {
    // console.log(result)
    // return error ? error : result
    // return result ? true : error
    if (result) {
      return true
    } else {
      return error
    }
  })
}

module.exports.login = (reqBody) => {
  const { email, password } = reqBody

  return User.findOne({ email: email }).then((result, error) => {
    // console.log(result)

    if (result == null) {
      console.log('email null')
      return false
    } else {
      let isPwCorrect = bcrypt.compareSync(password, result.password) //boolean bec it compares two arguments

      if (isPwCorrect == true) {
        //return json web tokenƒ
        //invoke the function which creates the token upon logging in
        // requirements in creating a token:
        // if password matches from existing pw from db
        return { access: auth.createAccessToken(result) }
        // return auth.createAccessToken(result)
      } else {
        return false
      }
    }
  })
}

module.exports.editProfile = (userId, reqBody) => {
  // console.log(userId)
  // console.log(reqBody)

  return User.findByIdAndUpdate(userId, reqBody, { new: true }).then(
    (result) => {
      // console.log(result)

      result.password = '****'

      return result
    }
  )
}

module.exports.editUser = (userId, reqBody) => {
  const { firstName, lastName, email, password, mobileNo, age } = reqBody

  const updatedUser = {
    firstName: firstName,
    lastName: lastName,
    email: email,
    mobileNo: mobileNo,
    age: age,
    password: bcrypt.hashSync(password, 10)
  }

  return User.findByIdAndUpdate(userId, updatedUser, { new: true }).then(
    (result) => {
      result.password = '****'
      return result
    }
  )
}

module.exports.editDetails = (reqBody) => {
  // console.log(reqBody)
  const { firstName, lastName, email, password, mobileNo, age } = reqBody

  const updatedUser = {
    firstName: firstName,
    lastName: lastName,
    email: email,
    mobileNo: mobileNo,
    age: age,
    password: bcrypt.hashSync(password, 10)
  }

  return User.findOneAndUpdate({ email: email }, updatedUser, {
    returnDocument: 'after'
  }).then((result) => {
    result.password = '****'

    return result
  })
}

module.exports.delete = (userId) => {
  // console.log(userId)

  return User.findByIdAndDelete(userId).then((result) => {
    // console.log(result)

    if (result) {
      return true
    }
  })
}

module.exports.deleteUser = (email) => {
  // console.log(email)

  return User.findOneAndDelete({ email: email }).then((result) => {
    if (result) {
      return true
    }
  })
}
