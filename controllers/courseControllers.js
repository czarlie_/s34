const Course = require('../models/Course')

module.exports.createCourse = (reqBody) => {
  const { courseName, description, price, isActive, createdOn } = reqBody

  const newCourse = new Course({
    courseName: courseName,
    description: description,
    price: price,
    isActive: isActive,
    createdOn: createdOn
  })

  return newCourse.save().then((result, error) => {
    return result ? true : error
  })
}

module.exports.getAllCourses = (reqBody) => {
  // console.log(reqBody)
  return Course.find(reqBody).then((result, error) => {
    return error ? error : result
  })
}
