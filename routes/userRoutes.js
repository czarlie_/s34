// create routes
const express = require('express')

// import to use .Router() method
// because .Router() is an express method
// Router() handles the requests
const router = express.Router()

// User Controller to use invoke()
const userController = require('../controllers/userController')
const auth = require('../auth')

router.get('/', (req, res) => {
  userController.getAllUsers().then((result) => res.send(result))
})

router.get('/details', auth.verify, (req, res) => {
  // console.log(req.headers.authorization);
  let userData = decode(req.headers.authorization)
  userController.getUserProfile(userData).then((result) => res.send(result))
})

//register route
router.post('/register', (req, res) => {
  //expecting data/object coming from request body
  userController.registerUser(req.body).then((result) => res.send(result))
})

router.post('/login', (req, res) => {
  userController.login(req.body).then((result) => res.send(result))
})

router.post('/email-exists', (req, res) => {
  //invoke the function here
  userController.checkEmail(req.body.email).then((result) => res.send(result))
})

router.put('/edit', auth.verify, (req, res) => {
  let userId = auth.decode(req.headers.authorization).id
  userController.editUser(userId, req.body).then((result) => res.send(result))
})

router.put('/edit-profile', (req, res) => {
  userController.editDetails(req.body).then((result) => res.send(result))
})

router.delete('/:userId/delete', (req, res) => {
  return userController
    .delete(req.params.userId)
    .then((result) => res.send(result))
})

router.put('/:userId/edit', (req, res) => {
  // console.log(req.params)
  // console.log(req.body)
  const userId = req.params.userId

  userController
    .editProfile(userId, req.body)
    .then((result) => res.send(result))
})

router.delete('/delete', (req, res) => {
  return userController
    .deleteUser(req.body.email)
    .then((result) => res.send(result))
})

module.exports = router
