const express = require('express')
const { modelNames } = require('mongoose')
const router = express.Router()
const auth = require('../auth')

const courseController = require('../controllers/courseControllers')

// 1.
//mini activity
// create a route to create a course with the following:
// endpoint = "/create-course" ✅
// function name: createCourse() ✅
// method: post ✅
// there should be a token checker ❌
// save the course in DB ✅
// return true if document successfully saved ✅
router.post('/create-course', (req, res) => {
  return courseController
    .createCourse(req.body)
    .then((result) => res.send(result))
})

// 2.
//mini activity
// create a route to get all courses with the following:
// endpoint = "/" ✅
// function name: getAllCourses() ✅
// method: get ✅
// use find() method ✅
// return all documents to the client ✅
router.get('/', (req, res) => {
  courseController.getAllCourses(req.body).then((result) => res.send(result))
})

// 3.
//mini activity
// create a route to update course's isActive status with the following:
// endpoint = "/:userId/archive"
// function name: archive()
// method: put
// use findByIdAndUpdate() method
// use { isActive: false } for the second parameter
// return true if successfully updated isActive status to false

// 4.
//mini activity
// create a route to update course's isActive status with the following:
// endpoint = "/:userId/unArchive"
// function name: unArchive()
// method: put
// use findByIdAndUpdate() method
// use { isActive: true } for the second parameter
// return true if successfully updated isActive status to true

// 5.
//mini activity
// create a route to get all "active" courses with the following:
// endpoint = "/active-courses"
// function name: getActiveCourses()
// method: get
// use find() method, use isActive field as filter
// return all documents that are active to the client

// 6.
//mini activity
// create a route to get a specific course with the following:
// endpoint = "/:courseId/specific-course"
// function name: getACourses()
// method: get
// use findById() method
// return the specific document to the client

// 7.
//mini activity
// create a route to update a course with the following:
// endpoint = "/edit-course"
// function name: editCourse()
// method: put
// use findByIdAndUpdate() method
// return updated version of the document to the client

// 8.
//mini activity
// create a route to delete a course with the following:
// endpoint = "/:userId/delete"
// function name: deleteCourse()
// method: delete
// use findByIdAndDelete() method
// return true if document successfully saved

module.exports = router
