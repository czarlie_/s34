// json we token
// sign - function that creates a token
// verify - function that checks if there's presence of token
// decode - this function that decodes the token

const jwt = require('jsonwebtoken')
const secret = 'MagandaAko'

module.exports.createAccessToken = (user) => {
  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin
  }
  return jwt.sign(data, secret, {})
}

module.exports.decode = (token) => {
  // jwt.decode(token, {});
  let slicedToken = token.slice(7, token.length)

  return jwt.decode(slicedToken)
}

module.exports.verify = (req, res, next) => {
  //where to get the token?
  let token = req.headers.authorization

  // jwt.verify(token, secret, function)

  if (typeof token !== 'undefined') {
    let slicedToken = token.slice(7, token.length)

    return jwt.verify(slicedToken, secret, (err, data) => {
      if (err) {
        res.send({ auth: 'failed' })
      } else {
        next()
      }
    })
  } else {
    res.send(false)
  }
}
